import { defineConfig } from 'vite'
import { nodePolyfills } from 'vite-plugin-node-polyfills'
import react from '@vitejs/plugin-react'
export default defineConfig({
  plugins: [react(), nodePolyfills()],
  server:{
    port:3001,
    host:'127.0.0.1',
    open: true,
    proxy: {
      "/iam/v1": {
        target: "https://apse1.dev.api.affinidi.io",
        changeOrigin: true,
        secure: false,
      },
      "/kyc/v1": {
        target: "https://apse1.dev.api.affinidi.io",
        changeOrigin: true,
        secure: false,
      },
    },
  }
})
