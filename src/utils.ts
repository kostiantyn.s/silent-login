import { nanoid } from 'nanoid'
import { PEX } from '@sphereon/pex/dist/browser'
import { OriginalVerifiableCredential } from '@sphereon/ssi-types'
import { KeysService, DidDocumentService } from '@affinidi/common'

import encode from 'base64url'

import { IPresentationDefinition as PresentationDefinition } from '@sphereon/pex'
import { PresentationSubmission } from '@sphereon/pex-models'
import {
  VerifiableCredential,
  VerifiablePresentation,
  WalletCredentials,
} from './features/wallet/types'
export async function matchVcsWithPresentationDefinition(input: {
  presentationDefinition: PresentationDefinition
  allVcs: VerifiableCredential[]
}): Promise<{
  matchedVcs: VerifiableCredential[]
  presentationSubmission: PresentationSubmission
}> {
  const pex = new PEX()
  const match = pex.selectFrom(
    input.presentationDefinition,
    input.allVcs as OriginalVerifiableCredential[],
  )

  if (
    !match.verifiableCredential ||
    match.areRequiredCredentialsPresent === 'error'
  ) {
    throw new Error('MISSING_VCS')
  }

  const matchedVcs: VerifiableCredential[] = []
  const vcs = match.verifiableCredential as VerifiableCredential[]
  for (const vc of vcs) {
    if (vc.id && matchedVcs.some((matchedVc) => matchedVc.id === vc.id)) {
      continue
    }
    matchedVcs.push(vc)
  }

  const presentationSubmission = pex.presentationSubmissionFrom(
    input.presentationDefinition,
    matchedVcs as OriginalVerifiableCredential[],
  )

  return { matchedVcs, presentationSubmission }
}

export async function generateVpTokenFromMatchedVcs(input: {
  matchedVcs: VerifiableCredential[]
  holderDid: string
}): Promise<{
  unsignedVpToken: VerifiablePresentation
}> {
  const unsignedVpToken: VerifiablePresentation = {
    id: `claimId:${nanoid()}`,
    type: ['VerifiablePresentation'],
    '@context': ['https://www.w3.org/2018/credentials/v1'],
    verifiableCredential: input.matchedVcs,
    holder: {
      id: input.holderDid,
    },
  }
  return { unsignedVpToken }
}

export async function reEncryptWalletCredentials(input: {
  walletCredentials: WalletCredentials
  encryptionKey: string
}): Promise<WalletCredentials> {
  const keysService = new KeysService(
    input.walletCredentials.encryptedSeed,
    input.walletCredentials.encryptionKey,
  )

  const { fullSeedHex: seedHex } = keysService.decryptSeed()
  const { encryptedSeed } = await KeysService.fromSeedAndPassword(
    seedHex,
    input.encryptionKey,
  )

  return {
    did: input.walletCredentials.did,
    encryptedSeed,
    encryptionKey: input.encryptionKey,
  }
}

function encodeObjectToJWT(object: {
  header: Record<string, string>
  payload: Record<string, unknown>
  signature: Buffer
}): string {
  const { header, payload, signature } = object

  if (!payload || !header || !signature) {
    throw new Error(
      'The JWT is not complete, header / payload / signature are missing',
    )
  }

  return [
    encode(JSON.stringify(header)),
    encode(JSON.stringify(payload)),
    encode(signature),
  ].join('.')
}

function signJWT(
  input: {
    header: Record<string, string>
    payload: Record<string, unknown>
  },
  ks: KeysService,
): string {
  const { header, payload } = input

  const toSign = [
    encode(JSON.stringify(header)),
    encode(JSON.stringify(payload)),
  ].join('.')

  const digest = KeysService.sha256(Buffer.from(toSign))
  const signature = ks.sign(digest)

  return encodeObjectToJWT({ header, payload, signature })
}

async function signAssertion(
  ks: KeysService,
  did: string,
  kid: string,
  aud: string,
) {
  const issueTimeS = Math.floor(new Date().getTime() / 1000)
  const payload = {
    iss: did,
    sub: did,
    aud,
    jti: nanoid(),
    exp: issueTimeS + 5 * 60,
    iat: issueTimeS,
  }
  const header = {
    alg: 'ES256K',
    kid,
  }

  return signJWT({ header, payload }, ks)
}

export async function getClientAssertion(input: {
  walletCredentials: WalletCredentials
  tokenUrl: string
}): Promise<string> {
  const { encryptedSeed, encryptionKey } = input.walletCredentials
  const keysService = new KeysService(encryptedSeed, encryptionKey)

  const keyDidService = DidDocumentService.createDidDocumentService(keysService)
  const did = keyDidService.getMyDid()
  const kid = keyDidService.getKeyId()

  const clientAssertion = await signAssertion(
    keysService,
    did,
    kid,
    input.tokenUrl,
  )

  return clientAssertion
}
