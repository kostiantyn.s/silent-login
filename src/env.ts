import { z } from 'zod'

export const centralApiGw = import.meta.env.VITE_CENTRAL_API_GW
export const vaultAuthorizationUrl = import.meta.env
  .VITE_VAULT_AUTHORIZATION_URL
export const appDistributionUrl = import.meta.env
  .VITE_DESKTOP_APP_DISTRIBUTION_URL
export const termsConditionsUrl = import.meta.env.VITE_TERMS_AND_CONDITIONS_URL
export const privacyNoticeUrl = import.meta.env.VITE_PRIVACY_NOTICE_URL
export const faqUrl = import.meta.env.VITE_FAQ_URL
export const affinidiUrl = import.meta.env.VITE_AFFINIDI_URL
export const vaultConsumerTokenUrl = import.meta.env
  .VITE_VAULT_CONSUMER_TOKEN_URL
export const vaultConsumerTokenAps1Url = import.meta.env
  .VITE_VAULT_CONSUMER_TOKEN_APS1_URL
export const acquisitionServicesUrl = import.meta.env
  .VITE_VAULT_ACQUISITION_SERVICES_URL
export const acquisitionServicesUiUrl = import.meta.env
  .VITE_VAULT_ACQUISITION_SERVICES_UI_URL
export const acquisitionServicesIotHost = import.meta.env
  .VITE_VAULT_ACQUISITION_SERVICES_IOT_HOST
export const acquisitionServicesCognitoPoolId = import.meta.env
  .VITE_VAULT_ACQUISITION_SERVICES_COGNITO_POOL_ID
export const supportUrl = import.meta.env.VITE_SUPPORT_URL

export const termsConditionsHash = import.meta.env.VITE_VAULT_TC_HASH

z.strictObject({
  centralApiGw: z.string().url().nonempty(),
  vaultAuthorizationUrl: z.string().url().nonempty(),
  appDistributionUrl: z.string().url().nonempty(),
  termsConditionsUrl: z.string().url().nonempty(),
  privacyNoticeUrl: z.string().url().nonempty(),
  faqUrl: z.string().url().nonempty(),
  termsConditionsHash: z.string().nonempty(),
  supportUrl: z.string().url().nonempty(),
}).parse({
  centralApiGw,
  vaultAuthorizationUrl,
  appDistributionUrl,
  termsConditionsUrl,
  privacyNoticeUrl,
  faqUrl,
  termsConditionsHash,
  supportUrl,
})
