import axios from 'axios'
import { VerifiableCredential } from '../../wallet/types'
import { centralApiGw } from '../../../env'

export class EmailVerification {
  constructor(private readonly host: string) {}

  async initiateOtp(
    email: string,
    accessToken: string,
  ): Promise<string | undefined> {
    const { data } = await axios({
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      method: 'POST',
      url: `/kyc/v1/otp/initiate`,
      data: { email },
    })

    return data?.challenge
  }

  async completeOtp(
    email: string,
    did: string,
    confirmationCode: string,
    accessToken: string,
    challenge?: string,
  ): Promise<VerifiableCredential> {
    const { data } = await axios({
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      method: 'POST',
      url: `/kyc/v1/otp/complete`,
      data: { email, did, confirmationCode, challenge },
    })

    return data.emailVc.signedCredential
  }
}

export const emailVerification = new EmailVerification(centralApiGw)
