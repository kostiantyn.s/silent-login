export class OAuthResponseError extends Error {
  constructor(readonly code: string) {
    super(`OAuth response error code: ${code}`)
  }
}
