import { Dispatch, FC, FormEvent, useState } from 'react'
import {
  Box,
  InputsNumerical,
  SetupCardButton,
  SetupCardPageLayout,
  ToastsContainer,
  Typography,
  toast,
} from '@affinidi/component-library'

import { Action, State } from '../Home'
import { emailVerification } from '../../email-verification/email-verification'
import { OAuthResponseError } from '../../errors'
import { theme } from '@affinidi/component-library'
import { AxiosError } from 'axios'

const CODE_LENGTH = 6

const oauthErrorMessageMap: Record<string, string> = {
  request_forbidden: 'Authorization request forbidden',
  invalid_request: 'Invalid authorization request',
  unauthorized_client: 'Unauthorized client',
  unsupported_response_type: 'Unsupported response type',
  invalid_scope: 'Invalid scope',
  access_denied: 'User has denied the request',
  server_error: 'Server error',
  temporarily_unavailable: 'Server is temporary unavailable',
}

export const ConfirmEmailStep: FC<{
  dispatch: Dispatch<Action>
  state: State
}> = ({ dispatch, state }) => {
  const [code, setCode] = useState('')
  const [challenge, setChallenge] = useState<string | undefined>(
    state.challenge,
  )
  const [error, setError] = useState<string>()
  const [isConfirming, setIsConfirming] = useState(false)

  const isOtpValid = code.length === CODE_LENGTH

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    setIsConfirming(true)
    setError(undefined)

    try {
      const emailVc = await emailVerification.completeOtp(
        state.email!,
        state.walletCredentials!.did,
        code,
        state.accessToken!,
        challenge,
      )

      dispatch({
        type: 'register',
        payload: {
          email: state.email!,
          emailVc,
          walletCredentials: state.walletCredentials!,
        },
      })

      setTimeout(() => {
        dispatch({ type: 'confirmEmail' })
      }, 1000)

      setCode('')
    } catch (error: any) {
      if (error instanceof OAuthResponseError) {
        setError(oauthErrorMessageMap[error.code] || error.message)
      } else if (error instanceof AxiosError) {
        setError(error?.response?.data.message)
      } else {
        setError(error.message)
      }
    } finally {
      setIsConfirming(false)
    }
  }

  async function handleReset() {
    dispatch({ type: 'reset' })
  }

  async function handleResendOtp() {
    try {
      const challenge = await emailVerification.initiateOtp(
        state.email!,
        state.accessToken!,
      )
      setChallenge(challenge)
      toast('Verification code has been resent', { type: 'success' })
    } catch (e) {
      toast('There was an error in sending the code. Please try again!', {
        type: 'error',
      })
    }
  }

  return (
    <>
      <SetupCardPageLayout
        cardTitle="Enter verification code"
        cardDescription={
          <>
            Enter the code that you received at <strong>{state.email}</strong>
          </>
        }
        cardFooter={
          <Footer handleReset={handleReset} handleResendOtp={handleResendOtp} />
        }
      >
        <form onSubmit={handleSubmit}>
          <InputsNumerical
            label="Verification code"
            codeLength={CODE_LENGTH}
            onChange={setCode}
            autoFocus
            error={!!error}
            helpText={error ? 'Code is incorrect, please try again.' : ''}
          />
          <SetupCardButton
            disabled={!isOtpValid || isConfirming}
            loading={isConfirming}
            type="submit"
          >
            Verify
          </SetupCardButton>
        </form>
      </SetupCardPageLayout>

      <ToastsContainer />
    </>
  )
}

const Footer = ({
  handleReset,
  handleResendOtp,
}: {
  handleReset: VoidFunction
  handleResendOtp: VoidFunction
}) => {
  return (
    <Box gap={16} direction="column">
      <Box gap={8}>
        <Typography>Didn’t receive an email?</Typography>
        <Typography
          id="resend_otp"
          style={{ color: `${theme.colors.brand.primary[90]}` }}
          onClick={handleResendOtp}
        >
          Resend
        </Typography>
      </Box>

      <Box gap={8}>
        <Typography>Wrong email?</Typography>
        <Typography
          id="change_email"
          style={{ color: `${theme.colors.brand.primary[90]}` }}
          onClick={handleReset}
        >
          Change email
        </Typography>
      </Box>
    </Box>
  )
}
