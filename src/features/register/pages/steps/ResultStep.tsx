import { FC, useEffect, useState } from 'react'

import { nanoid } from 'nanoid'
import {
  SetupCardButton,
  SetupCardPageLayout,
} from '@affinidi/component-library'
import { State } from '../Home'
import { generateKey } from '../../../crypto/generate-key'
import { termsConditionsHash } from '../../../../env'

export const ResultStep: FC<{ state: State }> = ({ state }) => {
  const [isSaved, setIsSaved] = useState(false)

  useEffect(() => {
    ;(async function save() {
      if (!state.pass || !state.walletCredentials || !state.emailVc) {
        throw new Error(
          'Incomplete state at the result step of the registration',
        )
      }

      const encryptionKey = state.pass
      const storageEncryptionKey = generateKey()

      localStorage.setItem(
        'data',
        JSON.stringify({
          installationId: nanoid(),
          encryptedSEK: storageEncryptionKey,
          encryptedData: {
            encryptionKey,
            data: {
              walletCredentials: state.walletCredentials,
              profileData: {},
              did: state.walletCredentials.did,
              vcs: [state.emailVc],
              unlockSetting: {
                type: 'timeout-and-on-restart',
                timeoutMs: 20 * 60_000,
              },
              consentedToTCHash: termsConditionsHash,
            },
            email: state.emailVc.credentialSubject.email,
            passInfo: {
              attempts: 0,
              lastAttemptedAt: undefined,
              unblocksAt: undefined,
            },
          },
        }),
      )

      setIsSaved(true)
    })()
  }, [state])

  if (!isSaved) {
    return <SetupCardPageLayout cardTitle="Just a moment..." />
  }

  return (
    <SetupCardPageLayout
      cardTitle="Passphrase created"
      cardDescription="Your passphrase is successfully saved. Explore your Affinidi Vault now or close this tab."
    >
      <SetupCardButton onClick={() => console.log('Go to Vault')} type="button">
        Explore my vault
      </SetupCardButton>
    </SetupCardPageLayout>
  )
}
