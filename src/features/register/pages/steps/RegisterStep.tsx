import { Dispatch, FC, FormEvent, useState } from 'react'
import {
  Box,
  Input,
  SetupCardPageLayout,
  SetupCardButton,
  ToastsContainer,
  toast,
} from '@affinidi/component-library'
import { Action } from '../Home'
import { createWallet } from '../../../../features/wallet/create-wallet'

import { emailVerification } from '../../email-verification/email-verification'
import { vaultConsumerTokenUrl } from '../../../../env'
import { getAccessToken } from '../../../oauth'

const EMAIL_REGEX = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/

export const RegisterStep: FC<{
  dispatch: Dispatch<Action>
}> = ({ dispatch }) => {
  const [email, setEmail] = useState('')
  const [isRegistering, setIsRegistering] = useState(false)
  const [hasEmailError, setEmailError] = useState<boolean>()

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const isEmailValid = EMAIL_REGEX.test(email)
    const hasError = !email || !isEmailValid

    if (hasError) {
      setEmailError(hasError)
      return
    }

    setIsRegistering(true)

    try {
      const wallet = await createWallet()
      console.log('wallet created')
      const walletCredentials = {
        did: wallet.did,
        encryptedSeed: wallet.encryptedSeed,
        encryptionKey: wallet.password,
      }
      const accessToken = await getAccessToken(
        walletCredentials,
        vaultConsumerTokenUrl,
      )
      const challenge = await emailVerification.initiateOtp(email, accessToken)

      dispatch({
        type: 'register',
        payload: {
          email,
          walletCredentials,
          accessToken,
          challenge,
        },
      })
    } catch (e) {
      console.error(e)
      toast('There was an error in sending the code. Please try again!', {
        type: 'error',
      })
    } finally {
      setIsRegistering(false)
    }
  }

  const handleEmailChange = (value: string) => {
    setEmail(value)
    setEmailError(undefined)
  }

  return (
    <>
      <SetupCardPageLayout
        cardTitle="Set up your Affinidi Vault"
        cardDescription="Please enter a valid email address to get started."
      >
        <form onSubmit={handleSubmit}>
          <Box gap={16} direction="column">
            <Input
              type="email"
              placeholder="example@affinidi.com"
              id="email"
              label="Email"
              value={email}
              onChange={handleEmailChange}
              disabled={isRegistering}
              required
              hasError={hasEmailError}
              helpText={hasEmailError ? 'Invalid email address' : undefined}
            />
          </Box>

          <SetupCardButton
            disabled={!email || hasEmailError || isRegistering}
            loading={isRegistering}
            type="submit"
          >
            Send verification code
          </SetupCardButton>
        </form>
      </SetupCardPageLayout>

      <ToastsContainer />
    </>
  )
}
