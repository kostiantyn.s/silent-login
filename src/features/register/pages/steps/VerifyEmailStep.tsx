import { Dispatch, FC, useState } from 'react'
import { Action } from '../Home'
import { SetupCardButton, SetupCardPageLayout } from '@affinidi/component-library'

export const VerifyEmailStep: FC<{
  dispatch: Dispatch<Action>
}> = ({ dispatch }) => {
  const [isVerified, setIsVerified] = useState(false)

  async function handleSubmit() {
    setIsVerified(true)
    dispatch({ type: 'verifyEmail' })
  }

  return (
    <SetupCardPageLayout
      cardTitle="Email verified"
      cardDescription="You've successfully created your Affinidi Vault. Please secure your account with a passphrase."
    >
      <SetupCardButton
        onClick={handleSubmit}
        loading={isVerified}
        type="button"
      >
        Create passphrase
      </SetupCardButton>
    </SetupCardPageLayout>
  )
}
