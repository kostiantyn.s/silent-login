import { Dispatch, FC, FormEvent, useState } from 'react'
import {
  Box,
  SetupCardButton,
  SetupCardPageLayout,
} from '@affinidi/component-library'

import { Action } from '../Home'

export const CreatePassStep: FC<{
  dispatch: Dispatch<Action>
}> = ({ dispatch }) => {
  const [pass, setPass] = useState('')
  const [passConfirm, setPassConfirm] = useState('')

  const isValid =
    pass.length >= 8 && passConfirm.length >= 8 && pass === passConfirm

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    if (!isValid) return
    dispatch({ type: 'createPass', payload: { pass: pass } })
  }

  return (
    <SetupCardPageLayout
      cardTitle="Create your passphrase"
      cardDescription="Please choose a passphrase. It will be used to unlock your vault. If you forget it, you will lose your access. Make sure to store it securely."
    >
      <form onSubmit={handleSubmit}>
        <Box gap={24} direction="column">
          <input
            autoFocus
            placeholder="Passphrase"
            onChange={(event) => setPass(event.target.value)}
          />
          <input
            placeholder="Confirm passphrase"
            onChange={(event) => setPassConfirm(event.target.value)}
          />
        </Box>
        <SetupCardButton disabled={!isValid} type="submit">
          Save passphrase
        </SetupCardButton>
      </form>
    </SetupCardPageLayout>
  )
}
