import { Dispatch, FC, Reducer, useLayoutEffect, useReducer, useState } from 'react'

import { RegisterStep } from './steps/RegisterStep'
import { ConfirmEmailStep } from './steps/ConfirmEmailStep'
import { CreatePassStep } from './steps/CreatePassStep'
import { ResultStep } from './steps/ResultStep'
import { VerifyEmailStep } from './steps/VerifyEmailStep'
import {
  VerifiableCredential,
  WalletCredentials,
} from '../../wallet/types.ts'

export type Action =
  | {
      type: 'register'
      payload: {
        email: string
        emailVc?: VerifiableCredential
        walletCredentials?: WalletCredentials
        accessToken?: string
        challenge?: string
      }
    }
  | {
      type: 'confirmEmail'
    }
  | {
      type: 'verifyEmail'
    }
  | {
      type: 'createPass'
      payload: {
        pass: string
      }
    }
  | {
      type: 'reset'
    }

export type State = {
  email?: string
  emailVc?: VerifiableCredential
  walletCredentials?: WalletCredentials
  isEmailConfirmed: boolean
  isEmailVerified: boolean
  pass?: string
  accessToken?: string
  challenge?: string
}

const initialState: State = {
  isEmailConfirmed: false,
  isEmailVerified: false,
}

const registrationReducer: Reducer<State, Action> = (
  state: State,
  action: Action,
): State => {
  switch (action.type) {
    case 'register':
      return { ...state, ...action.payload }
    case 'confirmEmail':
      return { ...state, isEmailConfirmed: true }
    case 'verifyEmail':
      return { ...state, isEmailVerified: true }
    case 'createPass':
      return { ...state, pass: action.payload.pass }
    case 'reset':
      return { ...initialState }
    default:
      throw new Error('Unsupported action type')
  }
}

export const Home: FC = () => {
  const [state, dispatch] = useReducer(registrationReducer, initialState)
  const [_isRegistered, setIsRegistered] = useState(false)

  useLayoutEffect(() => {
    const init = async () => {
      const isUserRegistered = localStorage.getItem('isRegistered')
      setIsRegistered(Boolean(isUserRegistered))
    }
  init()
  }, [])

  // if (!isInitialized) {
  //   return (
  //     <Box flex={1} alignItems="center" justifyContent="center">
  //       <Spinner />
  //     </Box>
  //   )
  // }

  return getStep(dispatch, state)
}

const getStep = (dispatch: Dispatch<Action>, state: State) => {
  if (!state.email) {
    return <RegisterStep dispatch={dispatch} />
  }

  if (!state.isEmailConfirmed) {
    return <ConfirmEmailStep dispatch={dispatch} state={state} />
  }

  if (!state.isEmailVerified) {
    return <VerifyEmailStep dispatch={dispatch} />
  }

  if (!state.pass) {
    return <CreatePassStep dispatch={dispatch} />
  }

  return <ResultStep state={state} />
}
