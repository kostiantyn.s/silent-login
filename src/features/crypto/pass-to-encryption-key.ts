import pbkdf2 from 'pbkdf2'

const ALGORITHM = 'sha256'
const ITERATIONS = 600_000
const LENGTH = 32

export async function passToEncryptionKey(pass: string, salt: string) {
  return new Promise<string>((resolve, reject) => {
    pbkdf2.pbkdf2(
      pass,
      salt,
      ITERATIONS,
      LENGTH,
      ALGORITHM,
      (error, derivedKey) =>
        error ? reject(error) : resolve(derivedKey.toString('hex')),
    )
  })
}
