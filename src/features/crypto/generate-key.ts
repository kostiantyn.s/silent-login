import { randomBytes } from 'crypto'
export const generateKey = () => {
  return randomBytes(32).toString('hex')
}
