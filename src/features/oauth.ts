import axios, { AxiosError } from 'axios'
import jwt_decode, { JwtPayload } from 'jwt-decode'
import { KeysService, DidDocumentService } from '@affinidi/common'
import encode from 'base64url'
import { WalletCredentials } from '../features/wallet/types'
import { nanoid } from 'nanoid'
function generateCodeVerifier() {
  const buffer = new Uint8Array(64)
  window.crypto.getRandomValues(buffer)
  const verifier = base64urlEncode(buffer)
  return verifier
}

export function base64urlEncode(buffer: Uint8Array) {
  const base64 = window.btoa(String.fromCharCode(...buffer))
  return base64.replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '')
}

async function sha256(message: string) {
  const data = new TextEncoder().encode(message)
  const hash = await window.crypto.subtle.digest('SHA-256', data)
  return base64urlEncode(new Uint8Array(hash))
}

export async function generateCodeVerifierAndChallenge(): Promise<{
  codeVerifier: string
  codeChallenge: string
}> {
  const codeVerifier = generateCodeVerifier()

  return {
    codeVerifier,
    codeChallenge: await sha256(codeVerifier),
  }
}

export function getIsTokenExpired(
  token: string,
  beforeMs: number = 0,
): boolean {
  const tokenData = jwt_decode<JwtPayload>(token)

  const expirationUnixTimeStamp = tokenData.exp

  if (!expirationUnixTimeStamp) {
    return true
  }

  const nowUnixTimestamp = (Date.now() + beforeMs) / 1000
  const isTokenExpired = expirationUnixTimeStamp - nowUnixTimestamp <= 0

  return isTokenExpired
}

async function callConsumerTokenEndpoint(
  clientAssertion: string,
  did: string,
  url: string,
) {
  const data = JSON.stringify({
    grant_type: 'client_credentials',
    client_assertion_type:
      'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
    client_assertion: clientAssertion,
    client_id: did,
  })
  const config = {
    method: 'post',
    maxBodyLength: Infinity,
    url,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    data,
  }

  try {
    return (await axios.request(config)).data
  } catch (e) {
    console.error((e as AxiosError)?.response?.data)
  }
}

export async function getAccessToken(
  walletCredentials: WalletCredentials,
  tokenUrl: string,
): Promise<string> {
  const clientAssertion = await getClientAssertion({
    walletCredentials,
    tokenUrl,
  })

  const accessToken = await callConsumerTokenEndpoint(
    clientAssertion,
    walletCredentials.did,
    'iam/v1/consumer/oauth2/token'
  )

  return accessToken.access_token
}

export async function getClientAssertion(input: {
  walletCredentials: WalletCredentials
  tokenUrl: string
}): Promise<string> {
  const { encryptedSeed, encryptionKey } = input.walletCredentials
  const keysService = new KeysService(encryptedSeed, encryptionKey)

  const keyDidService = DidDocumentService.createDidDocumentService(keysService)
  const did = keyDidService.getMyDid()
  const kid = keyDidService.getKeyId()

  const clientAssertion = await signAssertion(
    keysService,
    did,
    kid,
    input.tokenUrl,
  )

  return clientAssertion
}

async function signAssertion(
  ks: KeysService,
  did: string,
  kid: string,
  aud: string,
) {
  const issueTimeS = Math.floor(new Date().getTime() / 1000)
  const payload = {
    iss: did,
    sub: did,
    aud,
    jti: nanoid(),
    exp: issueTimeS + 5 * 60,
    iat: issueTimeS,
  }
  const header = {
    alg: 'ES256K',
    kid,
  }

  return signJWT({ header, payload }, ks)
}

function signJWT(
  input: {
    header: Record<string, string>
    payload: Record<string, unknown>
  },
  ks: KeysService,
): string {
  const { header, payload } = input

  const toSign = [
    encode(JSON.stringify(header)),
    encode(JSON.stringify(payload)),
  ].join('.')

  const digest = KeysService.sha256(Buffer.from(toSign))
  const signature = ks.sign(digest)

  return encodeObjectToJWT({ header, payload, signature })
}

function encodeObjectToJWT(object: {
  header: Record<string, string>
  payload: Record<string, unknown>
  signature: Buffer
}): string {
  const { header, payload, signature } = object

  if (!payload || !header || !signature) {
    throw new Error(
      'The JWT is not complete, header / payload / signature are missing',
    )
  }

  return [
    encode(JSON.stringify(header)),
    encode(JSON.stringify(payload)),
    encode(signature),
  ].join('.')
}
