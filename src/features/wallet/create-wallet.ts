import { AffinidiWalletV6, Types } from '@affinidi/wallet-browser-sdk'
import { options } from './options'
import { generateKey } from '../crypto/generate-key'

export async function createWallet(): Promise<Types.CommonNetworkMember> {
  return AffinidiWalletV6.createWallet(options, generateKey())
}
