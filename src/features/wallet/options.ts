import { Types } from '@affinidi/wallet-browser-sdk'

export const options: Types.SdkOptions = {
  env: 'dev',
  skipAnchoringForElemMethod: true,
  // TODO: this is required by SDK even though we don't really need it
  accessApiKey: 'fake-api-key-hash',
  didMethod: 'key'
}
