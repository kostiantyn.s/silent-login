export type WalletCredentials = {
  did: string
  encryptedSeed: string
  encryptionKey: string
}

export interface VerifiableCredential {
  '@context': string[]
  id: string
  type: string[]
  holder: { id: string }
  issuanceDate: string
  expirationDate?: string
  issuer?: string
  credentialSubject?: any
  credentialSchema?: {
    type: string
    id: string
  }
  proof?: any
}

export interface VerifiablePresentation {
  '@context': string[]
  id: string
  type: string[]
  holder: {
    id: string
  }
  verifiableCredential: VerifiableCredential[]
  proof?: any
}
