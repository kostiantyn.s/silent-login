import { AffinidiWalletV6 } from '@affinidi/wallet-browser-sdk'
import { options } from './options'
import {
  VerifiableCredential,
  VerifiablePresentation,
  WalletCredentials,
} from './types'

export async function signVc(
  walletCredentials: WalletCredentials,
  vc: VerifiableCredential,
): Promise<VerifiableCredential> {
  const wallet = await AffinidiWalletV6.openWalletByEncryptedSeed(
    options,
    walletCredentials.encryptedSeed,
    walletCredentials.encryptionKey,
  )

  return wallet.signUnsignedCredential(
    vc as any,
  ) as unknown as VerifiableCredential
}

export async function signVp(
  walletCredentials: WalletCredentials,
  input: {
    unsignedVp: VerifiablePresentation
    challenge: string
    domain: string
  },
) {
  const wallet = await AffinidiWalletV6.openWalletByEncryptedSeed(
    options,
    walletCredentials.encryptedSeed,
    walletCredentials.encryptionKey,
  )

  return wallet.signUnsignedPresentation(
    input.unsignedVp as any,
    input.challenge,
    input.domain,
  ) as unknown as VerifiablePresentation
}
